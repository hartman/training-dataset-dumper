#include "BrokenEventIndex.hh"
#include "H5Cpp.h"

BrokenEventWriter::BrokenEventWriter(H5::Group& output_file):
  m_offset(0)
{
  std::vector<hsize_t> ss{0};
  std::vector<hsize_t> sf{H5S_UNLIMITED};
  H5::DataSpace space(1,ss.data(),sf.data());
  H5::DSetCreatPropList params;
  std::vector<hsize_t> chk{500};
  params.setChunk(1, chk.data());
  params.setDeflate(7);

  auto stype = H5::StrType(H5::PredType::C_S1, H5T_VARIABLE);
  stype.setCset(H5T_CSET_UTF8);
  H5::CompType type(sizeof(BrokenEvent));
 #define INSERT(THING, TYPE)                         \
  type.insertMember(#THING, offsetof(BrokenEvent, THING), TYPE)
  INSERT(entry_number, H5::PredType::NATIVE_ULLONG);
  INSERT(event_number, H5::PredType::NATIVE_ULLONG);
  INSERT(jet_number, H5::PredType::NATIVE_UINT);
#define INSERT_NAMED(THING, TYPE, NAME)                         \
  type.insertMember(NAME, offsetof(BrokenEvent, THING), TYPE)
  INSERT_NAMED(_file, stype, "file");
  INSERT_NAMED(_jet_collection, stype, "jet_collection");
  INSERT_NAMED(_what, stype, "what");

  auto packed = H5::CompType(H5Tcopy(type.getId()));
  packed.pack();
  m_dataset = output_file.createDataSet(
    "broken_events", packed, space, params);
  m_type = type;
}

BrokenEventWriter::~BrokenEventWriter() {
  flush();
}

void BrokenEventWriter::add(const BrokenEvent& evt) {
  if (m_buffer.size() > 200) {
    flush();
  }
  m_buffer.push_back(evt);
}

unsigned long long BrokenEventWriter::size() const {
  return m_offset + m_buffer.size();
}

void BrokenEventWriter::flush() {
  if (m_buffer.size() == 0) return;
  unsigned long long old_size = m_buffer.size();
  unsigned long long new_size = m_offset + m_buffer.size();
  m_dataset.extend(&new_size);

  H5::DataSpace file_space = m_dataset.getSpace();
  H5::DataSpace mem_space(1, &old_size);
  file_space.selectHyperslab(H5S_SELECT_SET, &old_size, &m_offset);
  for (auto& evt: m_buffer) {
    evt._file = evt.file.data();
    evt._jet_collection = evt.jet_collection.data();
    evt._what = evt.what.data();
  }

  m_dataset.write(m_buffer.data(), m_type, mem_space, file_space);
  m_offset = new_size;
  m_buffer.clear();
}
