#include "ConfigFileTools.hh"

#include <boost/property_tree/json_parser.hpp>

namespace ConfigFileTools {
  bool boolinate(const boost::property_tree::ptree& pt,
                 const std::string key) {
    std::string val = pt.get<std::string>(key);
    if (val == "true") return true;
    if (val == "false") return false;
    throw std::logic_error("'" + key + "' should be 'true' or 'false'");
  }
  MapOfLists get_variable_list(const boost::property_tree::ptree& pt) {
    MapOfLists var_map;
    for (const auto& node: pt) {
      std::vector<std::string> vars;
      for (const auto& var: node.second) {
        vars.push_back(var.second.get_value<std::string>());
      }
      var_map[node.first] = vars;
    }
    return var_map;
  }

  // if any of the variable lists have a node called "file" we read in
// that file and replace the node with the contents.
  void combine_files(boost::property_tree::ptree& node,
                     boost::filesystem::path config_path) {
    namespace fs = boost::filesystem;
    namespace pt = boost::property_tree;
    if (!node.count("file")) return;
    pt::ptree old = node;
    fs::path file_path(node.get<std::string>("file"));

    // if the file doesn't exist in this directory, check the one
    // where the root configuration file was stored
    if (!fs::exists(file_path)) {
      file_path = config_path.parent_path() / file_path;
    }

    pt::ptree file;
    pt::read_json(file_path.string(), file);
    combine_files(file, file_path);
    node = file;

    // now add in anything else that was specified after the file
    for (auto& var_node: old) {
      for (auto& var: var_node.second) {
        node.get_child(var_node.first).push_back({"",var.second});
      }
    }
  }


}
