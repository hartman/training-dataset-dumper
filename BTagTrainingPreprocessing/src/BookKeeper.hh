#ifndef BOOKKEEPER_HH
#define BOOKKEEPER_HH

class TFile;
namespace xAOD {
  class TEvent;
}

struct Counts {
  Counts();
  unsigned long long nEventsProcessed;
  double sumOfWeights;
  double sumOfWeightsSquared;
  int nIncomplete;
  Counts& operator+=(const Counts& a);
};
Counts operator+(const Counts& a, const Counts& b);
Counts operator+=(const Counts& a, const Counts& b);

Counts get_counts(TFile& file, xAOD::TEvent& event);

#endif
