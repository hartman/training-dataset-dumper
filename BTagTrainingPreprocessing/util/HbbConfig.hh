#ifndef HBB_CONFIG_HH
#define HBB_CONFIG_HH

#include <string>
#include <vector>
#include <map>

struct SubjetConfig {
  std::string input_name;
  std::string output_name;
  bool get_subjets_from_parent;
  size_t n_subjets_to_save;
  size_t n_tracks;
  double min_jet_pt;
  std::map<std::string,std::vector<std::string>> variables;
  std::vector<std::string> nn_file_paths;
};



struct HbbConfig {
  std::vector<SubjetConfig> subjet_configs;
  std::string jet_collection;
  std::string jet_calib_file;
  std::string cal_seq;
  std::string cal_area;
  bool save_xbb_score;
  size_t n_const;
  std::string top_tag_config;
};

HbbConfig get_hbb_config(const std::string& config_file_name);

#endif
