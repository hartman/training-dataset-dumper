#include "HbbConfig.hh"
#include "ConfigFileTools.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>

HbbConfig get_hbb_config(const std::string& config_file_name) {
  namespace cft = ConfigFileTools;
  namespace fs = boost::filesystem;
  HbbConfig config;

  boost::property_tree::ptree pt;
  boost::property_tree::read_json(config_file_name, pt);

  for (auto& subnode: pt.get_child("subjets")) {
    auto& subjet = subnode.second;
    SubjetConfig cfg;
    cfg.input_name = subjet.get<std::string>("input_name");
    cfg.output_name = subjet.get<std::string>("output_name");
    cfg.get_subjets_from_parent = cft::boolinate(
      subjet,"get_subjets_from_parent");
    cfg.n_subjets_to_save = subjet.get<size_t>("n_subjets_to_save");
    cfg.n_tracks = subjet.get<size_t>("n_tracks");
    cfg.min_jet_pt = subjet.get<double>("min_jet_pt");
    // read in the b-tagging variables
    cft::combine_files(subjet.get_child("variables"), fs::path(config_file_name));
    cfg.variables = cft::get_variable_list(subjet.get_child("variables"));

    const std::string pathkey = "nn_file_paths";
    if (subjet.count(pathkey)) {
      for (const auto& paths: subjet.get_child(pathkey)) {
        cfg.nn_file_paths.push_back(paths.second.get_value<std::string>());
      }
    }

    config.subjet_configs.push_back(cfg);
  }
  config.jet_collection = pt.get<std::string>("jet_collection");
  config.jet_calib_file = pt.get<std::string>("jet_calib_file");
  config.cal_seq = pt.get<std::string>("cal_seq");
  config.cal_area = pt.get<std::string>("cal_area");
  config.save_xbb_score = cft::boolinate(pt, "save_xbb_score");
  config.n_const = pt.get<size_t>("n_const");
  config.top_tag_config = pt.get<std::string>("top_tag_config");

  return config;
}
