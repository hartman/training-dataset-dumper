#!/usr/bin/env python3

"""Script to update the lwtnn nameing convention

We want to move from the old hodgepodge to something more
consistent. See also AFT-411.

"""

from argparse import ArgumentParser
from sys import stdin, stdout
import json, re

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_file', nargs='?')
    parser.add_argument('-m', '--mapping-file', required=True)
    return parser.parse_args()

def run():
    args = get_args()
    input_file = open(args.input_file) if args.input_file else stdin
    network = json.load(input_file)
    inputs = network['inputs'].copy()
    if len(inputs) > 1:
        raise StandardError("can't handle more than one input right now")

    with open(args.mapping_file) as mapping_file:
        mapping = json.load(mapping_file)

    # first deal with the inputs
    input_translator = {old:new for old, new in mapping['inputs']}
    network['inputs'] = []
    for input_group in inputs:
        new_vars = []
        for input_object in input_group['variables']:
            old_name = input_object['name']
            new_name = input_translator.get(old_name, old_name)
            new_input = input_object.copy()
            new_input['name'] = new_name
            new_vars.append(new_input)
        network['inputs'].append({'name': 'btagging', 'variables': new_vars})

    # now the outputs
    label_translator = {old:new for old, new in mapping['output_labels']}
    node_translator = {old:new for old, new in mapping['output_nodes']}
    new_outputs = {}
    for key, out in network['outputs'].items():
        new_labels = [label_translator.get(x, x) for x in out['labels']]
        new_output = out.copy()
        new_output['labels'] = new_labels
        new_outputs[node_translator.get(key,key)] = new_output
    network['outputs'] = new_outputs

    json.dump(network, stdout, indent=2)

if __name__ == '__main__':
    run()

